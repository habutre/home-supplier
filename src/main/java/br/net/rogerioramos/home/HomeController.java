package br.net.rogerioramos.home;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class HomeController {
    
    public HomeController() {
        // TODO Auto-generated constructor stub
    }
    
    @RequestMapping("/about")
    public Home home() {
        return new Home("v0.0.1");
    }
}
