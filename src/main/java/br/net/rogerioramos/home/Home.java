package br.net.rogerioramos.home;

public class Home {

    private final static String ABOUT_MSG = "Welcome to HomeSupplier API %s";
    private final static String BITBUCKET_ISSUE_URL = "https://bitbucket.org/habutre/home-supplier/issues/new";
    private final static String HELP_MSG = String.format(
            "If you found any issue or need help, open an issue at %s",
            BITBUCKET_ISSUE_URL);
    private String welcome;
    private String help;

    public Home() {
    }

    public Home(String version) {
        this.setWelcome(version);
    }

    public Home(Long version) {
        this.setWelcome(version);
    }

    public Home(Integer version) {
        this.setWelcome(version);
    }

    public String getWelcome() {
        if (null == welcome)
            this.setWelcome();

        return welcome;
    }

    public void setWelcome() {
        this.welcome = String.format(ABOUT_MSG, "");
    }

    public void setWelcome(String version) {
        this.welcome = String.format(ABOUT_MSG, version);
    }

    public void setWelcome(Integer version) {
        this.setWelcome(String.valueOf(version));
    }

    public void setWelcome(Long version) {
        this.setWelcome(String.valueOf(version));
    }

    public String getHelp() {
        if (null == this.help)
            this.setHelp();

        return help;
    }

    public void setHelp() {
        this.help = HELP_MSG;
    }

    public void setHelp(String help) {
        this.help = help;
    }
}
